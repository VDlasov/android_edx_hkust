package hk.ust.cse.comp107x.chatclientnetworkfinal;

/**
 * Created by muppala on 11/7/15.
 */
public class Constants {

    public static final String SERVER = "http://192.168.1.63:3000/"; // "http://192.168.1.121:3000/";

    public static final String JSON_URL = SERVER + "people";
}
